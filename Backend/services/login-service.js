const { request, response, query } = require("express")

const jwt = require('jsonwebtoken');

module.exports = exports = (app, pool) => {
    app.post('/api/login', (request, response) => {
        let { email, password } = request.body

        const query = `SELECT * FROM users 
            WHERE email = '${email}' AND is_delete = 'false'`

        pool.query(query, (error, result) => {
            if (error) {
                response.send(400, {
                    success: false,
                    result: error
                })
            } else {
                if (result.rowCount < 1) {
                    response.send(400, {
                        success: false,
                        result: `Invalid email`
                    })
                } else {
                    if (result.rows[0].password == password) {
                        let payload = { subject: result.rows[0].id }
                        let jwttoken = jwt.sign(payload, 'secretKey')

                        response.send(200, {
                            success: true,
                            result: `Login ${email} success`,
                            token: jwttoken
                        })
                    } else {
                        response.send(401, {
                            success: false,
                            result: `Invalid password`
                        })
                    }
                }
            }
        })
    })
}