const { request, response, query } = require("express")

const jwt = require('jsonwebtoken');

module.exports = exports = (app, pool) => {
    app.post('/api/register', (request, response) => {
        let { email, password } = request.body

        const query = `SELECT id FROM users ORDER BY id DESC LIMIT 1`

        pool.query(query, (error, result) => {
            let idUser = 1
            if (result.rowCount == 0) {
                idUser
            } else {
                idUser = parseInt(result.rows[0].id) + 1
            }

            if (error) {
                response.send(400, {
                    success: false,
                    result: "error generate id user"
                })
            } else {
                const query = `INSERT INTO users(
                    email, password, create_by, create_date, is_delete)
                    VALUES ('${email}', '${password}', '${idUser}', now(), false);`

                pool.query(query, (error, result) => {
                    if (error) {
                        response.send(400, {
                            success: false,
                            result: error
                        })
                    } else {
                        let payload = { subject: result.rows[0].id }
                        let jwttoken = jwt.sign(payload, 'secretKey')
                        response.send(200, {
                            success: true,
                            result: `register ${email} success`,
                            token: jwttoken
                        })
                    }
                })
            }
        })
    })
}