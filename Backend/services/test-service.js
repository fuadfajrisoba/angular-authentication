const { request, response, query } = require("express")

const jwt = require('jsonwebtoken');

function verifyToken(request, response, next) {
    // console.log(request.headers.authorization);

    if (!request.headers.authorization) {
        return response.send(401, {
            success: false,
            result: "Unauthorize"
        })
    }

    let token = request.headers.authorization.split(' ')[1]
    if (token === 'null') {
        return response.status(401).send('Unauthorized request')
    }

    let payload = jwt.verify(token, 'secretKey')
    if (!payload) {
        return res.status(401).send('Unauthorized request')
    }
    request.userId = payload.subject
    next()

    // console.log(token);
}

module.exports = exports = (app, pool) => {
    app.get('/api/test', verifyToken, (request, response) => {

        const query = `SELECT * FROM test`

        pool.query(query, (error, result) => {
            if (error) {
                response.send(400, {
                    success: false,
                    result: error
                })
            } else {
                response.send(200, {
                    success: true,
                    result: result.rows
                })
            }
        })
    })
}