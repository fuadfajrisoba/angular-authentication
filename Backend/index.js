const express = require('express')
const bodyParser = require('body-parser')
const cors = require("cors")
const { request, response } = require('express')
global.config = require('./config/dbconfig')

const jwt = require('jsonwebtoken');

const app = express()
const port = 3000

app.use(cors())
app.use(bodyParser.json())
app.use(
    bodyParser.urlencoded({
        extended: true
    })
)

app.get('/', (request, response) => {
    response.json({ info: 'API Jalan' })
})

require('./services/test-service')(app, global.config.pool)
require('./services/register-service')(app, global.config.pool)
require('./services/login-service')(app, global.config.pool)

app.listen(port, () => {
    console.log(`App Running on port ${port}`)
})
